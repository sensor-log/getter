FROM openjdk:8
ADD target/getter.jar getter.jar
EXPOSE 80

ENV KAFKA_BOOTSTRAP_SERVERS=localhost:9092

ENTRYPOINT ["java", "-jar", "-Dspring.profiles.active=prod", "getter.jar"]