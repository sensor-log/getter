package com.hostingtest.getter.controller;

import com.hostingtest.getter.dto.TemperatureMetricaDTO;
import com.hostingtest.getter.service.PublicSetvice;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping("/metrica")
public class MetricaController {

    private PublicSetvice publicSetvice;

    @PostMapping("/temperature")
    public ResponseEntity setTemperature(@RequestBody TemperatureMetricaDTO metricaDTO) {
        publicSetvice.publicTemperature(metricaDTO);
        return ResponseEntity.ok().build();
    }

}
