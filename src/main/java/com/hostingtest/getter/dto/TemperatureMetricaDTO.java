package com.hostingtest.getter.dto;

import lombok.Data;

@Data
public class TemperatureMetricaDTO {
    private Long timestamp;
    private Double metrica;
}
