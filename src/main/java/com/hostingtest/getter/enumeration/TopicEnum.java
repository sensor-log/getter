package com.hostingtest.getter.enumeration;

import lombok.Getter;

@Getter
public enum TopicEnum {

    TEMPERATURE("TEMPERATURE-1");

    private String title;

    TopicEnum(String title) {
        this.title = title;
    }

}
