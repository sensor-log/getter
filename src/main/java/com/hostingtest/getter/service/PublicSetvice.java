package com.hostingtest.getter.service;

import com.hostingtest.getter.dto.TemperatureMetricaDTO;
import com.hostingtest.getter.enumeration.TopicEnum;
import lombok.AllArgsConstructor;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;

@Service
@AllArgsConstructor
public class PublicSetvice {

    private KafkaTemplate<String, String> kafkaTemplate;

    public void publicTemperature(TemperatureMetricaDTO metricaDTO) {
        String row = metricaDTO.getTimestamp() + "," + metricaDTO.getMetrica();
        kafkaTemplate.send(TopicEnum.TEMPERATURE.getTitle(), row);
    }

}
